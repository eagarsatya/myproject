﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BelajasASPNET___Practice.API
{
    [ApiController]
    [Route("api/book")]
    public class BookAPI : Controller
    {

        private readonly ServiceBooks DB;

        //Bind di sini
        public BookAPI(ServiceBooks serviceBooks)
        {
            DB = serviceBooks;
        }


        [HttpGet(Name = "get-all-book")]
        public async Task<ActionResult<List<BukuViewModel>>> GetAllBook()
        {
            var allBook = await DB.GetBookAsync();

            if (allBook == null)
            {
                return NotFound();
            }

            return allBook;
        }

        [HttpGet("{id}", Name = "get-book-by-id")]
        public async Task<ActionResult<BukuViewModel>> GetBookByID(Guid id)
        {
            var allBook = await DB.GetBookAsync();

            if (allBook == null)
            {
                return NotFound();
            }

            var searchBook = allBook.Find(Q => Q.ID == id);

            if (searchBook == null)
            {
                return NotFound();
            }

            return searchBook;
        }

        [HttpPost(Name = "insert-book")]
        public async Task<ActionResult> Post([FromBody]BukuViewModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            await DB.CreateBookAsync(model);
            return Ok();
        }

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}
        [HttpPost("{id}", Name = "update-book")]
        public async Task<ActionResult> Update([FromBody]BukuViewModel model, Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var allBooks = await DB.GetBookAsync();
            var updateBook = allBooks.FindIndex(Q => Q.ID == id);

            if (updateBook == -1)
            {
                return NotFound();
            }

            allBooks[updateBook] = model;

            await DB.UpdateListBooks(allBooks);
            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}", Name = "delete-book")]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var allBooks = await DB.GetBookAsync();
            var deleteBook = allBooks.Find(Q => Q.ID == id);

            if (deleteBook == null)
            {
                return NotFound();
            }

            allBooks.Remove(deleteBook);

            await DB.UpdateListBooks(allBooks);
            return Ok();
        }
    }
}
