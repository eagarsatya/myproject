﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BelajasASPNET___Practice.API
{
    [Route("api/v1/Customer")]
    [ApiController]
    [Produces("application/json")]
    public class CustomerAPIController : Controller
    {
        private readonly CustomerService _CustomerService;
        private readonly IConfiguration _Config;

        public CustomerAPIController(CustomerService customerService, IConfiguration configuration)
        {
            _CustomerService = customerService;
            _Config = configuration;
        }

        [HttpGet("ApiKeyGetAllCustomer", Name = "get-all-customer")]
        public async Task<ActionResult<List<CustomerViewModel>>> GetAllCustomerApi()
        {
            var apiKey = Request.Headers["ApiKey"];
            if(string.IsNullOrEmpty(apiKey) || apiKey != _Config["ApiKey"])
            {
                return BadRequest("Invalid API Key");
            }
            if(ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var allData = await _CustomerService.GetAllCustomerLinq();
            if(allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        [HttpPost("ApiKeyInsertCustomer",Name = "insert-customer")]
        public async Task<ActionResult> InsertCustomer([FromBody]CustomerViewModel model)
        {
            var apiKey = Request.Headers["ApiKey"];
            if (string.IsNullOrEmpty(apiKey) || apiKey != _Config["ApiKey"])
            {
                return BadRequest("Invalid API Key");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            await _CustomerService.InsertCustomer(model);
            return Ok();
        }

        [HttpGet("ApiKeyFindCustomerById/{id}",Name = "find-customer-by-id")]
        public async Task<ActionResult<CustomerViewModel>> GetCustomerByIdAsync(int id)
        {
            var apiKey = Request.Headers["ApiKey"];
            if (string.IsNullOrEmpty(apiKey) || apiKey != _Config["ApiKey"])
            {
                return BadRequest("Invalid API Key");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var deleteUser = await _CustomerService.FindCustomerById(id);

            if(deleteUser == null)
            {
                return null;
            }

            return deleteUser;

        }

        [HttpPost("ApiKeyUpdateCustomer/{id}",Name = "update-customer-by-id")]
        public async Task<ActionResult> UpdateCustomerAsync([FromBody] CustomerViewModel model,int id)
        {
            var apiKey = Request.Headers["ApiKey"];
            if (string.IsNullOrEmpty(apiKey) || apiKey != _Config["ApiKey"])
            {
                return BadRequest("Invalid API Key");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var findUser = await _CustomerService.FindCustomerById(id);

            if (findUser == null)
            {
                return BadRequest("There is no Customer with this id");
            }

            await this._CustomerService.UpdateCustomer(model);

            return Ok();
        }

        [HttpDelete("ApiKeyDeleteCustomer/{id}",Name = "delete-customer")]
        public async Task<ActionResult> DeleteCustomer(int id)
        {
            var apiKey = Request.Headers["ApiKey"];
            if (string.IsNullOrEmpty(apiKey) || apiKey != _Config["ApiKey"])
            {
                return BadRequest("Invalid API Key");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var deleteUser = await _CustomerService.FindCustomerById(id);

            await _CustomerService.DeleteCustomer(deleteUser);
            return Ok();
        }


    }
}
