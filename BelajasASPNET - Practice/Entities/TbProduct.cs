﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelajasASPNET___Practice.Entities
{
    public partial class TbProduct
    {
        public TbProduct()
        {
            TbPurchase = new HashSet<TbPurchase>();
        }

        public int ProductId { get; set; }
        [StringLength(20)]
        public string ProductName { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal? ProductPrice { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<TbPurchase> TbPurchase { get; set; }
    }
}
