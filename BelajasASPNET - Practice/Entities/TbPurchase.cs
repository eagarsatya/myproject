﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelajasASPNET___Practice.Entities
{
    public partial class TbPurchase
    {
        public int PurchaseId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public DateTimeOffset? PurchaseDate { get; set; }

        [ForeignKey("CustomerId")]
        [InverseProperty("TbPurchase")]
        public virtual TbCustomer Customer { get; set; }
        [ForeignKey("ProductId")]
        [InverseProperty("TbPurchase")]
        public virtual TbProduct Product { get; set; }
    }
}
