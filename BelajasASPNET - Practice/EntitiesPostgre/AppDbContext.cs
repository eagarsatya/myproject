﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BelajasASPNET___Practice.EntitiesPostgre
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbCustomer> TbCustomer { get; set; }
        public virtual DbSet<TbProduct> TbProduct { get; set; }
        public virtual DbSet<TbPurchase> TbPurchase { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Host=localhost;Database=DatabasePurchase;Username=postgres;Password=aoak1725");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<TbCustomer>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("TbCustomer_pkey");
            });

            modelBuilder.Entity<TbProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("TbProduct_pkey");
            });

            modelBuilder.Entity<TbPurchase>(entity =>
            {
                entity.HasKey(e => e.PurchaseId)
                    .HasName("TbPurchase_pkey");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("TbPurchase_CustomerId_fkey");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("TbPurchase_ProductId_fkey");
            });
        }
    }
}
