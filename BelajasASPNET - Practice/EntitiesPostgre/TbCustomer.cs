﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelajasASPNET___Practice.EntitiesPostgre
{
    public partial class TbCustomer
    {
        public TbCustomer()
        {
            TbPurchase = new HashSet<TbPurchase>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        [InverseProperty("Customer")]
        public virtual ICollection<TbPurchase> TbPurchase { get; set; }
    }
}
