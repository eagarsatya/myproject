﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BelajasASPNET___Practice.EntitiesPostgre
{
    public partial class TbProduct
    {
        public TbProduct()
        {
            TbPurchase = new HashSet<TbPurchase>();
        }

        public int ProductId { get; set; }
        [Column(TypeName = "numeric")]
        public decimal? ProductPrice { get; set; }
        public string ProductName { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<TbPurchase> TbPurchase { get; set; }
    }
}
