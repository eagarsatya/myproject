﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajasASPNET___Practice.Models
{
    public class BarangViewModel
    {
        //public int IdBarang { get; set; }
        public string NamaBarang { get; set; }
        public decimal HargaBarang { get; set; }
    }
}
