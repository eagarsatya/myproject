﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajasASPNET___Practice.Models
{
    public class BukuViewModel
    {
        public Guid ID { get; set; }
        public string Judul { get; set; }
        public string Kategori { get; set; }
        public decimal Harga { get; set; }
    }
}
