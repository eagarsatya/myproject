using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace BelajasASPNET___Practice.Pages.Auth
{
    public class LoginModel : PageModel
    {
        public class FormLogin
        {
            [Required(ErrorMessage = "Tidak boleh kosong")]
            public string Email { set; get; }
            [Required]
            public string Password { set; get; }
        }

        [BindProperty]
        public FormLogin Form { set; get; }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync(string returnUrl,[FromServices]IConfiguration configuration)
        {
            FormLogin formLogin = new FormLogin
            {
                Email = configuration["accelist"],
                Password = configuration["AdminPassword"]
            };

            if (ModelState.IsValid == false)
            {
                return Page();
            }

            // cek ke database, apakah login bener?!?
            // don't do this at home
            //var hash = "$2a$12$A8qcqbkdr/Xt7VTIncoZ1eclz8nBwYzDf8DkesjJNIXaphT51e/vG"; //HelloWorld!
            var valid = Form.Email == formLogin.Email && Form.Password == formLogin.Password;
            if (valid == false)
            {
                ModelState.AddModelError("Form.Password", "Invalid login email or password!");
                return Page();
            }

            var id = new ClaimsIdentity("BelajarLogin");
            id.AddClaim(new Claim(ClaimTypes.Name, Form.Email));
            id.AddClaim(new Claim(ClaimTypes.Email, Form.Email));
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, Form.Email)); // user ID / username / PK
            id.AddClaim(new Claim(ClaimTypes.Role, "Administrator"));

            var principal = new ClaimsPrincipal(id);
            await this.HttpContext.SignInAsync("BelajarLogin", principal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(5),
                IsPersistent = true
            });

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToPage("/Index");
        }
    }
}