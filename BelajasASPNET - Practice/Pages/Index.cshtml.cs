﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using BelajasASPNET___Practice.Services;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace BelajasASPNET___Practice.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {

        private readonly IConfiguration _Config;

        public List<BarangViewModel> ListBarang { set; get; }

        public IndexModel(IConfiguration configuration)
        {
            this._Config = configuration;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            ListBarang = new List<BarangViewModel>();
            var koneksi = _Config.GetConnectionString("ConnString");
            var query = "SELECT NamaBarang, HargaBarang FROM TbBarang";
            using (var connection = new SqlConnection(koneksi))
            {
                var result = (await connection.QueryAsync<BarangViewModel>(query)).ToList();
                ListBarang = result;
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var koneksi = _Config.GetConnectionString("ConnString");
            var spName = "AddorEditBarang";
            //var query = "INSERT INTO TbBarang Values (@nmBarang, @hargaBarang)";
            using (var connection = new SqlConnection(koneksi))
            {
                var affectedRow = await connection.ExecuteAsync(spName,
                    new
                    {
                        idBarang = 0,
                        namaBarang = "Sedan",
                        hargaBarang = 10000
                    },
                    commandType: CommandType.StoredProcedure);
            }

            return RedirectToPage();
        }

        //public async Task<IActionResult> OnPostAsync()
        //{
        //    var koneksi = _Config.GetConnectionString("ConnString");
        //    var query = "INSERT INTO TbBarang Values (@nmBarang, @hargaBarang)";
        //    using (var connection = new SqlConnection(koneksi))
        //    {
        //        var affectedRow = await connection.ExecuteAsync(query,
        //            new
        //            {
        //                nmBarang = "a1",
        //                hargaBarang = 1
        //            });
        //    }

        //    return RedirectToPage();
        //}
    }
}
