using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    public class AddBukuModel : PageModel
    {

        //private readonly BookServices DB;
        private readonly ServiceBooks DB;

        public AddBukuModel(ServiceBooks data)
        {
            DB = data;
        }

        public class AddBukuForm
        {
            [Required]
            public string Judul { get; set; }
            [Required]
            public string Kategori { get; set; }
            [Required]
            public decimal Harga { get; set; }
        }

        [BindProperty]
        public AddBukuForm form { get; set; }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPost()
        {
            if(ModelState.IsValid == false)
            {
                return Page();
            }

            await DB.CreateBookAsync(new BukuViewModel {
                ID = Guid.NewGuid(),
                Judul = form.Judul,
                Kategori = form.Kategori,
                Harga = form.Harga
            });

            return RedirectToPage("/ViewBuku/RedisIndex");

        }
    }
}