using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    public class AddBukuApiModel : PageModel
    {

        private readonly IHttpClientFactory httpFac;

        public AddBukuApiModel(IHttpClientFactory recieve)
        {
            httpFac = recieve;
        }

        [BindProperty]
        public BukuViewModel form { set; get; }

        public void OnGet()
        {
        }

        public async Task<ActionResult> OnPost()
        {
            var apiUrl = "http://localhost:3250/api/book";
            var client = httpFac.CreateClient();
            var respond = await client.PostAsJsonAsync(apiUrl,new BukuViewModel {
                Harga = form.Harga,
                ID = Guid.NewGuid(),
                Kategori = form.Kategori,
                Judul = form.Judul
            });

            if(respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to insert book");
            }

            return Page();
        }

    }
}