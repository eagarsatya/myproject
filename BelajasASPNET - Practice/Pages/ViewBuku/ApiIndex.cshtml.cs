using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    [Authorize]
    public class ApiIndexModel : PageModel
    {
        private readonly IHttpClientFactory httpFac;

        public ApiIndexModel(IHttpClientFactory httpClient)
        {
            httpFac = httpClient;
        }

        [BindProperty]
        public List<BukuViewModel> allBook { set; get; }

        public async Task<IActionResult> OnGet()
        {
            var apiUrl = "http://localhost:3250/api/book/";
            var client = httpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to get data");
            }

            allBook = await respond.Content.ReadAsAsync<List<BukuViewModel>>();
            return Page();
        }
    }
}