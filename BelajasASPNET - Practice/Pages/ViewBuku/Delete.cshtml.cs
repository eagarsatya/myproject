using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    public class DeleteModel : PageModel
    {

        //private readonly BookServices DB;
        private readonly ServiceBooks DB;

        public DeleteModel(ServiceBooks data)
        {
            DB = data;
        }

        public class DeleteForm
        {
            public string Judul { get; set; }
            public string Kategori { get; set; }
            public decimal Harga { get; set; }
        }

        [BindProperty]
        public DeleteForm form { get; set; }

        [BindProperty(SupportsGet = true)]
        public Guid ID { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            var allBooks = await DB.GetBookAsync();
            var searchDelete = allBooks.Find(Q => Q.ID == ID);

            if (searchDelete == null)
            {
                return NotFound();
            }

            form = new DeleteForm
            {
                Judul = searchDelete.Judul,
                Kategori = searchDelete.Kategori,
                Harga = searchDelete.Harga
            };

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            //Delete
            //var searchDelete = DB.listBuku.Find(Q => Q.ID == ID);

            var allBooks = await DB.GetBookAsync();
            var searchDelete = allBooks.Find(Q => Q.ID == ID);

            allBooks.Remove(searchDelete);

            //DB.listBuku.Remove(searchDelete);

            await DB.UpdateListBooks(allBooks);

            return RedirectToPage("/ViewBuku/RedisIndex");

        }

    }
}