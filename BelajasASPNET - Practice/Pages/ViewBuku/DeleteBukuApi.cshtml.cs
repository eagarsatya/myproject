using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    public class DeleteBukuApiModel : PageModel
    {
        private IHttpClientFactory httpFac;

        public DeleteBukuApiModel(IHttpClientFactory httpClient)
        {
            httpFac = httpClient;
        }

        [BindProperty]
        public BukuViewModel form { get; set; }

        [BindProperty(SupportsGet = true)]
        public Guid id { get; set; }

        public async Task OnGet()
        {
            var apiUrl = "http://localhost:3250/api/book/";
            var client = httpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl + id.ToString());

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to get book");
            }

            var book = await respond.Content.ReadAsAsync<BukuViewModel>();

            form = new BukuViewModel
            {
                Harga = book.Harga,
                ID = book.ID,
                Judul = book.Judul,
                Kategori = book.Kategori
            };

        }

        public async Task<IActionResult> OnPost()
        {
            var apiUrl = "http://localhost:3250/api/book/";
            var client = httpFac.CreateClient();
            var respond = await client.DeleteAsync(apiUrl + id.ToString());

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to delete book");
            }

            return RedirectToPage("/ViewBuku/ApiIndex");
        }
    }
}