﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    public class DropDownKategori
    {
        public string Category { get; set; }

        static public List<SelectListItem> Categories { get; } = new List<SelectListItem>
        {
            new SelectListItem{Value = "Sci-fi", Text = "Sci-fi"},
            new SelectListItem{Value = "Romance", Text = "Romance"},
            new SelectListItem{Value = "Advanture", Text = "Advanture"}
        };
    }
}
