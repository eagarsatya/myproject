using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    public class MainIndexModel : PageModel
    {
        [BindProperty]
        public List<BukuViewModel> allBook { get; set; }

        private readonly ServiceBooks DB;

        public MainIndexModel(ServiceBooks recievedDB)
        {
            DB = recievedDB;
        }

        public async Task OnGet()
        {
            allBook = await DB.GetBookAsync();
        }
    }
}