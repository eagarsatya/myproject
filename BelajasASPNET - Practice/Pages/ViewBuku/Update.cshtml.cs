using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajasASPNET___Practice.Pages.ViewBuku
{
    public class UpdateModel : PageModel
    {

        //private readonly BookServices DB;
        private readonly ServiceBooks DB;

        public class UpdateForm
        {
            [Required]
            public string Judul { get; set; }
            [Required]
            public string Kategori { get; set; }
            [Required]
            public decimal Harga { get; set; }
        }

        [BindProperty]
        public UpdateForm form { get; set; }

        [BindProperty(SupportsGet = true)]
        public Guid ID { get; set; }

        public UpdateModel(ServiceBooks data)
        {
            DB = data;
        }

        public async Task<IActionResult> OnGet()
        {
            var allBooks = await DB.GetBookAsync();
            var search = allBooks.Find(Q => Q.ID == ID);

            if (search == null)
            {
                return NotFound();
            }

            form = new UpdateForm
            {
                Judul = search.Judul,
                Harga = search.Harga,
                Kategori = search.Kategori
            };

            return Page();

        }

        public async Task<IActionResult> OnPostAsync()
        {
            var allBooks = await DB.GetBookAsync();
            var search = allBooks.Find(Q => Q.ID == ID);

            if (search == null)
            {
                return NotFound();
            }

            if(ModelState.IsValid == false)
            {
                return Page();
            }

            search.Judul = form.Judul;
            search.Kategori = form.Kategori;
            search.Harga = form.Harga;

            await DB.UpdateListBooks(allBooks);

            return RedirectToPage("/ViewBuku/RedisIndex");
        }
    }
}