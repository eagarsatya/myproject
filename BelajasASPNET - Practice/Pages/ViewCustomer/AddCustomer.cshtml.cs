using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace BelajasASPNET___Practice.Pages.ViewCustomer
{
    public class AddCustomerModel : PageModel
    {

        private readonly IHttpClientFactory _HttpClientFactory;
        private readonly IConfiguration _Config;

        public AddCustomerModel(IHttpClientFactory httpClientFactory,IConfiguration configuration)
        {
            this._HttpClientFactory = httpClientFactory;
            this._Config = configuration;
        }

        [BindProperty]
        public CustomerViewModel FormCustomer { get; set; }

        public void OnGet()
        {
        }

        public async Task<ActionResult> OnPostAsync()
        {
            CustomerViewModel insertCustomer = new CustomerViewModel
            {
                CustomerName = FormCustomer.CustomerName
            };
            var apiUrl = "http://localhost:3250/api/v1/Customer/ApiKeyInsertCustomer";
            var client = _HttpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Add("ApiKey", _Config["ApiKey"]);
            var respond = await client.PostAsJsonAsync(apiUrl, insertCustomer);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to insert customer");
            }

            return RedirectToPage("/ViewCustomer/IndexCustomer");
        }
    }
}