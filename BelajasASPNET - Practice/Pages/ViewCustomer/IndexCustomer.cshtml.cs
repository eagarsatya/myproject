using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace BelajasASPNET___Practice.Pages.ViewCustomer
{
    public class IndexCustomerModel : PageModel
    {

        private readonly IHttpClientFactory _HttpClientFactory;
        private readonly IConfiguration _Config;

        public IndexCustomerModel(IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            this._HttpClientFactory = httpClientFactory;
            this._Config = configuration;
        }

        public List<CustomerViewModel> ListCustomer;

        [BindProperty]
        public int CustomerId { get; set; }
        public CustomerViewModel FormCustomer { get; set; }

        public async Task OnGet()
        {
            ListCustomer = new List<CustomerViewModel>();

            var apiUrl = "http://localhost:3250/api/v1/Customer/ApiKeyGetAllCustomer";
            var client = _HttpClientFactory.CreateClient();

            client.DefaultRequestHeaders.Add("ApiKey", _Config["ApiKey"]);

            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to get data");
            }

            ListCustomer = await respond.Content.ReadAsAsync<List<CustomerViewModel>>();

        }

        public async Task<ActionResult> OnPostAsync()
        {
            await OnGet();
            FormCustomer = new CustomerViewModel();
            var apiUrl = "http://localhost:3250/api/v1/Customer/ApiKeyFindCustomerById/" + CustomerId.ToString();
            var client = _HttpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Add("ApiKey", _Config["ApiKey"]);
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to get data");
            }

            FormCustomer = await respond.Content.ReadAsAsync<CustomerViewModel>();
            return Page();
        }
    }
}