using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace BelajasASPNET___Practice.Pages.ViewCustomer
{
    public class UpdateCustomerModel : PageModel
    {
        private readonly IHttpClientFactory _HttpClientFactory;
        private readonly IConfiguration _Config;

        public UpdateCustomerModel(IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            this._HttpClientFactory = httpClientFactory;
            this._Config = configuration;
        }

        [BindProperty]
        public CustomerViewModel FormCustomer { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CustomerId { get; set; }

        public async Task OnGetAsync()
        {
            FormCustomer = new CustomerViewModel();
            var apiUrl = "http://localhost:3250/api/v1/Customer/ApiKeyFindCustomerById/";
            var client = _HttpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Add("ApiKey", _Config["ApiKey"]);
            var respond = await client.GetAsync(apiUrl + CustomerId.ToString());

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to get data");
            }

            FormCustomer = await respond.Content.ReadAsAsync<CustomerViewModel>();
        }

        public async Task<ActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:3250/api/v1/Customer/ApiKeyUpdateCustomer/" + CustomerId.ToString();
            var client = _HttpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Add("ApiKey", _Config["ApiKey"]);
            var respond = await client.PostAsJsonAsync(apiUrl,new CustomerViewModel {
                CustomerId = CustomerId,
                CustomerName = FormCustomer.CustomerName
            });

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Failed to Update Customer");
            }

            return RedirectToPage("/ViewCustomer/IndexCustomer");
        }
    }
}