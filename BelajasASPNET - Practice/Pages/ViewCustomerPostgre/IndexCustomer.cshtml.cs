using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Models;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace BelajasASPNET___Practice.Pages.ViewCustomerPostgre
{
    public class IndexCustomerModel : PageModel
    {
        private readonly IConfiguration _Config;
        private readonly CustomerServicePostgre _CustomerServicePostgre;

        public IndexCustomerModel(CustomerServicePostgre customerServicePostgre, IConfiguration configuration)
        {
            this._CustomerServicePostgre = customerServicePostgre;
            this._Config = configuration;
        }

        public List<CustomerViewModel> ListCustomer;

        [BindProperty]
        public int CustomerId { get; set; }
        public CustomerViewModel FormCustomer { get; set; }

        public async Task OnGet()
        {
            ListCustomer = new List<CustomerViewModel>();

            var allCustomer = _CustomerServicePostgre.GetAllCustomerLinq();
            ListCustomer = await allCustomer;

        }

        //public async Task<ActionResult> OnPostAsync()
        //{

        //}
    }
}