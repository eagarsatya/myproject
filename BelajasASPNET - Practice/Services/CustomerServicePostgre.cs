﻿using BelajasASPNET___Practice.EntitiesPostgre;
using BelajasASPNET___Practice.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajasASPNET___Practice.Services
{
    public class CustomerServicePostgre
    {
        private readonly AppDbContext _AppDbContext;

        public List<CustomerViewModel> ListCustomer { set; get; }



        public CustomerServicePostgre(AppDbContext appDbContext)
        {
            this._AppDbContext = appDbContext;
        }

        public async Task<List<CustomerViewModel>> GetAllCustomerLinq()
        {
            var getAllData = await this._AppDbContext.TbCustomer
                .AsNoTracking()
                .Select(Q => new CustomerViewModel
                {
                    CustomerId = Q.CustomerId,
                    CustomerName = Q.CustomerName
                }).ToListAsync();

            return getAllData;
        }
        public async Task<bool> InsertCustomer(CustomerViewModel model)
        {
            var addData = new TbCustomer()
            {
                CustomerName = model.CustomerName
            };
            this._AppDbContext.TbCustomer.Add(addData);
            await this._AppDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateCustomer(CustomerViewModel model)
        {
            var findUser = await this._AppDbContext.TbCustomer.FirstOrDefaultAsync(Q => Q.CustomerId == model.CustomerId);
            if (findUser == null)
            {
                return false;
            }
            findUser.CustomerName = model.CustomerName;
            this._AppDbContext.TbCustomer.Update(findUser);
            await this._AppDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteCustomer(CustomerViewModel model)
        {
            var deleteUser = await this._AppDbContext.TbCustomer.FirstOrDefaultAsync(Q => Q.CustomerId == model.CustomerId);
            if (deleteUser == null)
            {
                return false;
            }
            this._AppDbContext.TbCustomer.Remove(deleteUser);
            await this._AppDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<CustomerViewModel> FindCustomerById(int id)
        {
            var findUser = await this._AppDbContext.TbCustomer.FirstOrDefaultAsync(Q => Q.CustomerId == id);
            if (findUser == null)
            {
                return null;
            }

            CustomerViewModel user = new CustomerViewModel
            {
                CustomerId = findUser.CustomerId,
                CustomerName = findUser.CustomerName
            };

            return user;
        }
    }
}
