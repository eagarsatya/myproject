﻿using BelajasASPNET___Practice.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajasASPNET___Practice.Services
{
    public class ServiceBooks
    {

        private readonly IDistributedCache Cache;

        public ServiceBooks(IDistributedCache distributedCache)
        {
            Cache = distributedCache;
        }
        public async Task<List<BukuViewModel>>GetBookAsync()
        {
            var valueJson = await Cache.GetStringAsync("Books");

            if(valueJson == null)
            {
                return new List<BukuViewModel>();
            }

            return JsonConvert.DeserializeObject<List<BukuViewModel>>(valueJson);
        }

        public async Task CreateBookAsync(BukuViewModel buku)
        {
            var allBooks = await GetBookAsync();

            if(allBooks == null)
            {
                allBooks = new List<BukuViewModel>();
            }

            allBooks.Add(buku);
            var jsonListBooks = JsonConvert.SerializeObject(allBooks);
            await Cache.SetStringAsync("Books", jsonListBooks);

        }

        public async Task UpdateListBooks(List<BukuViewModel> books)
        {

            var jsonAllBooks = JsonConvert.SerializeObject(books);

            await Cache.SetStringAsync("Books", jsonAllBooks);

        }
    }
}
