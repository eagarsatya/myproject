using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajasASPNET___Practice.Entities;
using BelajasASPNET___Practice.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace BelajasASPNET___Practice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services
                .AddAuthentication("BelajarLogin")
                .AddCookie("BelajarLogin", option =>
                {
                    option.LoginPath = "/auth/login";
                    option.LogoutPath = "/auth/logout";
                    option.AccessDeniedPath = "/auth/denied";
                    option.SessionStore = new RedisTicketStore(new Microsoft.Extensions.Caching.StackExchangeRedis.RedisCacheOptions
                    {
                        Configuration = "localhost",
                        InstanceName = "BelajarLogin"
                    });
                });

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "BelajarAsp";
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Manage Book", Version = "v1" });
            });

            services.AddHttpClient();

            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("ConnString"), strategy =>
                 {
                     strategy.EnableRetryOnFailure();
                 });
            });

            services.AddDbContext<EntitiesPostgre.AppDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("PostgreConnString"))
            );

            //services.AddEntityFrameworkNpgsql()
            //   .AddDbContext<AppDbContext>()
            //   .BuildServiceProvider();

            services.AddSingleton<BookServices>();
            services.AddScoped<ServiceBooks>();
            services.AddScoped<CustomerService>();
            services.AddScoped<CustomerServicePostgre>();

            services.AddSession();
            services.AddHttpContextAccessor();
            services.AddHttpClient();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSession();
            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Manage Book");
                c.DisplayOperationId();
            });

            app.UseMvc();
        }
    }
}
